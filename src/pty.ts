
import Child from 'child_process'
import { EventEmitter } from 'events'

export class PtyProcess extends EventEmitter{
    $p: Child.ChildProcess
    constructor(process: Child.ChildProcess){
        super()
        this.$p = process

        this.$init()
    }

    get pid(){
        return this.$p.pid
    }

    resize(){

    }

    write(data, cb?){
        this.$p.stdin.write(Buffer.from(data), cb)
    }

    $init(){
        let er = (e)=> this.emit("error",e)
        this.$p.on("error", er)
        this.$p.stdout.on("error", er)
        this.$p.stderr.on("error", er)

        this.$p.stdout.on("data", (data)=> this.emit("data", data))
        this.$p.stderr.on("data", (data)=> this.emit("data", data))
        this.$p.on("exit", (code)=> this.emit("exit",code))

        //this.$p.stdin.resume && this.$p.stdin.resume()
    }
}



export class Pty{
    
    spawn(cmd: string, args: string[], options: any){
        let noptions:any = {}
        if(options.env){
            noptions.env = options.env 
        }
        if(options.cwd){
            noptions.cwd = options.cwd 
        }
        
        let p = Child.spawn(cmd, args, noptions)
        return new PtyProcess(p)   
    }


}


export async function kawixPreload(){

    let pty =  null
    exports.child = new Pty()
    try{
        pty = await import('npm://node-pty-prebuilt-multiarch@0.10.1-pre.5')
    }catch(e){
        pty = exports.child          
    }
    exports.pty = pty

}