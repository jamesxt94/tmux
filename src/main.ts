import {Process, Tmux as tmux} from './Tmux.ts'
import Child from 'child_process'
import * as async from "gh+/kwruntime/std@1.1.4/util/async.ts"
import {Exception} from "gh+/kwruntime/std@1.1.4/util/exception.ts"
export var Tmux = tmux
import fs from 'fs'


main()
async function main(){
	process.on("uncaughtException", function(e){
		console.info("Unhandled exception:", e.stack)
	})

	console.info()
	console.info("KMUX Service")
	try{
		await execute()
	}catch(e){
		console.error("[ERROR]", e.message || e)
		process.exit(1)
	}
}


async function execute(){



	let params:any = {}
	for(let i=1;i<kawix.appArguments.length;i++){
		let arg = kawix.appArguments[i]
		let parts = arg.split("=")
		let name = parts[0].substring(2)

		let value = parts.slice(1).join("=") || ''
		params[name] = value
		params[name+"_Array"] = params[name+"_Array"] || []
		params[name+"_Array"].push(value)
	}


	if(params.user){
		process.env.KMUX_USER = params.user
	}


	if(params.log){
		let stream = fs.createWriteStream(params.log, {
			flags: "a"
		})
		stream.on("error", ()=>{})
		let original = {
			stdout: process.stdout.write ,
			stderr: process.stderr.write
		}
		process.stdout.write = function(bytes, cb){
			bytes = Buffer.from(bytes)
			let args = [bytes]
			if(typeof cb == "function")
				args.push(cb)
			let v = original.stdout.apply(process.stdout, args)
			stream.write(bytes)
			return v
		}

		process.stderr.write = function(bytes, cb){
			bytes = Buffer.from(bytes)
			let args = [bytes]
			if(typeof cb == "function")
				args.push(cb)
			let v = original.stderr.apply(process.stderr, args)
			stream.write(bytes)
			return v
		}
	}


	let pro: Process
	let exitProcess = async function(code){
		//console.info("PRO:", pro)
		/*
		if(pro){
			console.info("Clearing variable 'pro'")
			await pro.rpa_unref()
		}*/
		process.exit(code)
	}


	let id = params.id || "default"
	let tmux = new Tmux(id)
	let startDetached = async function(){
		let args = [process.argv[1], process.argv[2], "--server"]

		if(params.id){
			args.push("--id="+params.id)
		}
		if(params.user){
			args.push("--user="+params.user)
		}
		if(params["background-log"]){
			args.push("--log=" + params["background-log"])
		}
		console.info("Starting server in background mode")
		let p = Child.spawn(process.argv[0], args, {
			stdio: 'ignore',
			env: Object.assign({}, process.env),
			detached: true
		})
		p.unref()
		await async.sleep(2000)
	}


	let startService = async function(){
		if(params.detached !== undefined){
			await startDetached()
			exitProcess(0)
		}
		if(params.background !== undefined){
			await startDetached()
			exitProcess(0)
		}
		else{
			let ctmux = await tmux.checkLocalServer()
			let yetStarted = true
			if(!ctmux){
				yetStarted = false
				try{
					await tmux.localServer()
					console.info("Started local server")

					// wait some seconds??
					setTimeout(async function(){
						await tmux.init()
					}, 5000)

				}
				catch(e){
					console.error("Failed starting server:", e.message)
					process.exit(1)
				}
			}
			if(params.remote !== undefined){
				await (ctmux ? ctmux.client : tmux).remoteServer(params.password || "admin", params.hostname || "0.0.0.0", params.port || 42000)
			}
			if(yetStarted){
				exitProcess(0)
			}
		}
	}

	if(params.boot !== undefined){
		params.server = true
		params.detached = true
	}

	if(params.server !== undefined){
		if(params.close === undefined && params.restart === undefined){
			return await startService()
		}
	}







	let name = params["process-id"] || params.name
	let channel = null
	if((params.list !== undefined) || (params.memory !== undefined) || (params["info"] !== undefined) || (params["delete"] !== undefined) || (params["save"] !== undefined) || (params["restart"] !== undefined) || (params["close"] !== undefined) || (params["hot-update"] !== undefined) || name){

		try{
			try{
				if(params.host){
					channel = await Tmux.connectRemote(params.port || 42000, params.host, params.password)
				}
				else{
					channel = await Tmux.connect(id)
				}
			}catch(e){

				if(!params.host){

					if(params.autostart === "0"){
						console.error("> Local/Remote KMux service not found")
						process.exit(1)
					}
					// execute server detached
					await startDetached()
					await async.sleep(2000)
				}

				if(params.host){
					channel = await Tmux.connectRemote(params.port || 42000, params.host, params.password)
				}
				else{
					channel = await Tmux.connect(id)
				}
			}
		}catch(ex){
			throw Exception.create("Cannot connect to local/remote KMux service.")
		}
	}



	if(params["info"] !== undefined){
		tmux = channel.client
		//console.info("Source=", await tmux.getSourceFile())
		console.info(`Source=\x1b[34m${await tmux.getSourceFile()}\x1b[0m Version=\x1b[34m${await tmux.getVersion()}\x1b[0m`)
	}

	if(params["hot-update"] !== undefined){
		tmux = channel.client
		console.info("Applying hot update to service")
		await tmux.hotUpdate(params["hot-update"])
		console.info(`Updating info: Source=\x1b[34m${await tmux.getSourceFile()}\x1b[0m Version=\x1b[34m${await tmux.getVersion()}\x1b[0m`)
	}

	if(params.kill !== undefined){
		tmux = channel.client
		let pros = params["process-id_Array"] || params.name_Array || []
		if(!pros.length){
			console.error("Need specify --process-id")
			process.exit(1)
		}
		let ds = params.force !== undefined ? "SIGKILL" : "SIGTERM"
		for(let pid of pros){
			await tmux.kill(pid, params.signal || ds)
		}
	}

	if(params.delete !== undefined){
		tmux = channel.client
		let pros = params["process-id_Array"] || params.name_Array || []
		if(!pros.length){
			console.error("Need specify --process-id")
			process.exit(1)
		}
		let ds = params.force !== undefined ? "SIGKILL" : "SIGTERM"
		for(let pid of pros){
			await tmux.delete(pid)
		}
	}

	if(params.list !== undefined){
		if(channel){
			tmux = channel.client
			console.info("Process list")
			//console.info()
			let processes = await tmux.getProcesses()
			for(let i=0;i <processes.length;i++){
				let pro = processes[i]
				let c = '\x1b[32m', d= ' '
				if(pro.status == "offline"){
					c = '\x1b[31m'
					d= ''
				}
				console.info(`Status= ${c}${pro.status}\x1b[0m${d} Name= \x1b[33m${pro.id}\x1b[0m  Command= \x1b[34m${pro.cmd}\x1b[0m Arguments= \x1b[34m${pro.args.join(", ")}\x1b[0m`)
			}

		}
		exitProcess(0)
	}

	if(params.memory !== undefined){
		if(channel){
			tmux = channel.client
			let mem = await tmux.getMemoryUsage()
			console.info("Memory usage:", mem)
		}
		exitProcess(0)
	}

	if(params.save !== undefined){
		tmux = channel.client
		await tmux.save()
		console.info("Process list saved")
		exitProcess(0)
	}

	if(params.start){
		tmux = channel.client
		let pid = params["process-id"] || params.name
		let pro = await tmux.getProcess(pid)
		if(pro){
			console.error(`Process ${pid} is yet started.`)
		}
		pro = await tmux.createProcess(pid || Date.now().toString(32))
		if(params.pty){
			await pro.selectPTY(params.pty)
		}
		await pro.start(params.start, params.arg_Array || [])
		if(params.write_Array){
			for(let s of params.write_Array)
				await pro.write(Buffer.from(s + "\n").toString('base64'))
		}
		exitProcess(0)
	}

	if(params.close !== undefined && params.server !== undefined ){
		tmux = channel.client
		console.info("Closing service and all processes")
		await tmux.close()
	}



	if(params.restart !== undefined){
		tmux = channel.client
		if(params.server !== undefined){

			console.info("Closing service and all processes")
			await tmux.close()
			await async.sleep(100)
			console.info("Starting service")

			params.detached = true
			await startService()
			return
		}


		let pid = params["process-id"] || params.name
		if(!pid){
			return console.error(`Process id|name not specified.`)
		}

		let pro = await tmux.getProcess(pid)
		if(!pro){
			console.error(`Process ${pid} is not started.`)
			return exitProcess(1)
		}
		console.info(`Restarting process ${pid}`)
		await tmux.restart(pid)
		//let info = await pro.getStartInfo()
		//await pro.stop()
		exitProcess(0)
	}




	if(params.connect !== undefined || params.log !== undefined){
		tmux = channel.client
		let pid = params["process-id"] || params.name
		let pro = await tmux.getProcess(pid)
		if(!pro){
			console.error(`Process ${pid} is not started.`)
			return exitProcess(1)
		}
		let rpid =  await pro.getPid()
		console.info("Pid=", rpid, "Name=", await pro.id())
		let data = Buffer.from(await pro.getLastBuffer(),'base64')
		process.stdout.write(data)

		pro.on("data", function(base64){
			let bytes = Buffer.from(base64,'base64')
			process.stdout.write(bytes)
		})

		pro.on("exit", function(){
			console.info("Process was terminated. Pid=", rpid)
			return exitProcess(1)
		})
		try{
			await pro.resize(process.stdout.columns, process.stdout.rows)
		}catch(e){
			console.error("[ERROR] Failed Resize:", e)
		}

		process.stdout.on("resize", function(){
			console.info(arguments)
		})
		process.stdin.setRawMode(true)
		process.stdin.on("data", async (b)=>{
			let text = b.toString('base64')
			let ctrlc = text == "Aw=="
			if(pid.startsWith("shell")){
				ctrlc = false
			}
			if(text == "BA==" || ctrlc){
				//tmux.destroy()
				console.info("")
				console.info("Disconnected from process console!")
				return exitProcess(0)
			}

			try{
				await pro.write(b.toString('base64'))
			}catch(e){
				if(e.code == "RPA_DESTROYED"){
					exitProcess(2)
				}
			}
		})
		process.stdin.resume()
		return
	}


	exitProcess(0)
}
