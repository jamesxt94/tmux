import * as async from "gh+/kwruntime/std@1.1.19/util/async.ts"
import {Exception} from "gh+/kwruntime/std@1.1.19/util/exception.ts"
import {AsyncEventEmitter} from "gh+/kwruntime/std@1.1.19/async/events.ts"
//import { shareSymbol, Tmux} from "./tmux"
import Os from 'os'
export const shareSymbol = Symbol("share")
import { ProcessHelper } from "./linux/process.ts"
import Path from 'path'
import fs from 'fs'

export class ProcessInfo{
	id: string 
	status?: string 
	cmd: string 
	args: string[]
	env: {[key: string]: string}
	autorestart?: boolean

	spawner?: string
	kernel_status? : {[key:string]: string}
	memory_usage?: {
		rss: number
		childrenRss: number 
		total:number
	}
}



export class Process extends AsyncEventEmitter{

	$internal: {[key: string]: any} = {}

    #info: ProcessInfo
    #p: any 
	#tmux: Tmux
	#buffers = new Array<Buffer>()
	#status = new Map<string, string>()
	#pty : any
    
    get info(){
        return this.#info
    }

	set info(value){
		this.#info = value
	}

	getStatus(id?:string ){
		if(id){
			return this.#status.get(id)
		}
		else{
			let o:any = {}
			let keys = this.#status.keys()
			for(let key of keys){
				o[key] = this.#status.get(key)
			}
			return o
		}
	}

	getLastBuffer(){
		let all = []
		let b = [].concat(this.#buffers)
		let len = 0, offset = b.length - 1
		while(len < 1000){
			let bu = this.#buffers[offset]
			offset --
			if(!bu) break
			len += bu.length
			all.push(bu)
		}
		all.reverse()
		let bytes = Buffer.concat(all)
		return bytes.toString('base64')
	}

	write(data: Buffer | string){
		if(typeof data == "string"){
			data = Buffer.from(data,'base64')
		}
		this.#p.write(data)
	}


	resize(cols:number, rows:number){
		this.#p.resize(cols,rows)
	}

	get pid(){
		return this.#p.pid
	}

    constructor(tmux){
		
        super()
		this.#tmux = tmux
		this[shareSymbol] = true
	}
	

	#markExit(){
		this.#info.status = 'offline'
		this.emit("exit")
	}

	$markExit(){
		this.#info.status = 'offline'
		this.emit("exit")
		this.#p.pid = 0
	}

    getAsyncEnumerator(event: string | string[]): AsyncGenerator<any, void, unknown> {
		let enum1 = this.getEnumerator(event)
		enum1[shareSymbol] = true 
		return enum1
	}


	async stop(force = false, timeout= 10000){

		try{
			await this.kill('SIGTERM', timeout)
		}
		catch(e){
			// failed close normally 
			if(Os.platform() != "win32" && force){
				await this.kill('SIGKILL', timeout)
			}
			else{
				throw e 
			}
		}

	}


	async kill(signal = 'SIGTERM', timeout = 10000){
		let def = new async.Deferred<void>()
		let in0 = setTimeout(() => def.reject(Exception.create("Timeout closing process").putCode("TIMEOUT")), timeout)
		try{			
			process.kill(this.#p.pid, signal)
			this.#p.once("exit", def.resolve)
			//this.#p.once("error", def.reject)
			await def.promise
		}catch(e){
			this.#markExit()
		}
		finally{
			clearTimeout(in0)
		}
	}


	async start(){

		this.emit("starting", this.#info)
		this.#tmux.emit("process-starting", this.#info)

		//let def = new async.Deferred<void>()
		let pty = this.#tmux.spawners[this.#info.spawner || "pty"]
		let cmd = this.#info.cmd 
		if(Os.platform() == "win32"){
			if(!Path.isAbsolute(cmd)){
				let discovered = false
				let paths = process.env.PATH.split(Path.delimiter)
				for(let path of paths){
					let pref = Path.join(path, cmd)
					let files = [pref, pref+".exe", pref+".bat", pref+".cmd"]
					for(let file of files){
						if(fs.existsSync(file)){
							cmd = file
							discovered = true  
							break 
						}
					}
					if(discovered) break
				}

			}		
		}
		let p = this.#p = pty.spawn(cmd, this.#info.args, {
			name: 'xterm-color',
			cols: 80,
			rows: 30,
			env: Object.assign({}, process.env, this.#info.env)
		})
		//p.once("error", def.reject)
		p.on("error", (e) => console.error("> Error on child process:", e.message))
		p.on("data", (bytes) => {

			let p = Buffer.from(bytes)			
			let lines = p.toString().split(/\r?\n/ig)
			for(let line of lines){
				let i = line.indexOf("kmux:status:")
				if(i >= 0){
					let parts = line.substring(i+12).split("$$")[0].split(":")
					this.#status.set(parts[0], parts[1])
					this.emit("status:" + parts[0], parts[1])
					this.#tmux.emit("status:"+ parts[0], {
						id: this.#info.id,
						data: parts[1]
					})
				}
			}
			
			this.#buffers.push(p)
			while(this.#buffers.length > 200){
				this.#buffers[0] = null
				this.#buffers.shift()
			}
			this.emit("data", p.toString('base64'))
		})
		p.once("exit", () => this.#markExit())
		delete this.info.kernel_status 
		delete this.info.memory_usage
		if(Os.platform() == "linux"){
			try{
				this.info.kernel_status = await ProcessHelper.getStatus(p.pid)
				this.info.memory_usage = await ProcessHelper.getMemoryUsage(p.pid)
			}catch(e){}
		}

		this.info.status = 'online'
	}

}






