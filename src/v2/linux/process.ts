import fs from 'fs'
import * as async from "gh+/kwruntime/std@1.1.19/util/async.ts"
export class ProcessHelper{

    static async getStatus(pid: number){

        let content = await fs.promises.readFile(`/proc/${pid}/status`, "utf8")
        let lines = content.split(/\r?\n/g)
        let status: {[key:string]:string} = {}
        for(let item of lines){
            let i = item.indexOf(":")
            let name = item.substring(0, i)
            let value = item.substring(i+1)

            status[name.trim()] = value.trim()
        }

        return status
    } 

    static async getChildrens(pid: number){
        let taskids = await fs.promises.readdir(`/proc/${pid}/task`)
        let tasks = new Array<async.DelayedTask<string>>()
        for(let id of taskids){
            tasks.push(new async.DelayedTask<string>(fs.promises.readFile(`/proc/${pid}/task/${id}/children`, 'utf8')))
        }
        let children = new Array<number>()
        while(tasks.length){
            let content = await (tasks.shift().promise)
            let pids = content.split(" ").filter(Boolean).map(Number)
            children.push(...pids)
        }   
        return children
    }

    static async getMemoryUsage(pid:number){
        // get total mem from pid include all childrens 
        let status = await this.getStatus(pid)
        let usage = {
            rss: 0,
            childrenRss: 0,
            total: 0
        }
        usage.rss = Number(status.VmRSS.match(/(\d+)/)[1]) * 1024
        usage.total = usage.rss
        let childrens = await this.getChildrens(pid)

        for(let cpid of childrens){
            let result = await this.getMemoryUsage(cpid)
            usage.childrenRss += result.rss
            usage.total += result.total
        }
        /*
        usage.rss = Math.round(usage.rss)
        usage.childrenRss = Math.round(usage.childrenRss)
        usage.total = Math.round(usage.childrenRss)
        */
        return usage
    }

}