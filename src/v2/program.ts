import {Tmux as TmuxClass} from './tmux.ts'
//import {Process} from './process.ts'
import * as async from "gh+/kwruntime/std@1.1.18/util/async.ts"
import {Exception} from "gh+/kwruntime/std@1.1.18/util/exception.ts"

import {Server} from 'gitlab://jamesxt94/mesha@2.0.3/src/server.ts'
import {Client} from 'gitlab://jamesxt94/mesha@2.0.3/src/clients/default.ts'
import {RPC} from 'gitlab://jamesxt94/mesha@2.0.3/src/rpc.ts'


import {parse} from 'gitlab://jamesxt94/codes@f99c48bd/cli-params.ts'
import ChildProcess from 'child_process'
import Os from 'os'

function username(){
	let user = process.env.USER 
	if(Os.platform() == "win32") user = Os.userInfo().username
	return user 
}



export class Program{



	async bind(id: string, address?: string, password?: string){
		
		let getId = (id: string) => `${username()}.kmux-${id}`

		let tmux = new TmuxClass(id)
		tmux.on("process-starting", (info) => {
			console.info("> Starting process:", info.cmd, "Arguments=", info.args.join(" "))
		})
		tmux.on("closing", () => {
			console.info("> Closing server")
		})

		let shared = function(pass: string){
			if(!password){
				return tmux 
			}else if(password == pass){
				return tmux 
			}
		}

		setTimeout(async function(){
			await tmux.init()
		}, 2000)


		let server = new Server()
		server.shared.set("getObject", shared)	
		server.on("error", console.error)
		server.on("rpc", (rpc: RPC)=>{
			console.info("[RPC] New connection received")
		})
		
		if(!address){
			address = "local+" + getId(id)
		}
		return await server.bind(address)
	}

	async main(args: Array<string>){

		

		let cli = parse(args)
		if(cli.params.base64){
			let params = JSON.parse(Buffer.from(cli.params.base64,"base64").toString())
			Object.assign(cli.params, params)
			delete cli.params.base64
			for(let id in cli.params){
				let value = cli.params[id]
				if(value instanceof Array){
					cli.paramsArray[id] = value 
				}
				else{
					cli.paramsArray[id] = [value]
				}
			}
		}


		let getId = (id: string) => `${username()}.kmux-${id}`
		let connect = async (address: string, password?: string)=>{
			try{

				let client = new Client()
				client.on("error", console.error)
				await client.connect(address)
				
				const rpc = client.rpc 
				let getObject = await rpc.defaultScope.remote("getObject")
				let tmux: TmuxClass = await getObject.invoke(password)
				return tmux 
			}catch(e){
				if(e.code == "ENOENT" || e.code =="EREFUSED"){
					return null
				}
				throw e 
			}
		}

		let connectIdOrAddress = async (params:any)=>{
			try{


				let id = getId(cli.params.id || "default")
				let address = cli.params.address || cli.params.bind
				if(!address){
					address = "local+" + id
				}


				return await connect(address, cli.params.password)

			}catch(e){
				throw e 
			}
		}


		if(cli.params.server !== undefined){			
			let keys = ["address","id","bind","password", "restart", "start", "server", "close", "background"]
			for(let id in cli.params){
				if(keys.indexOf(id) <0)
					return console.error("> Invalid parameter:", id)
			}

			if(cli.params.restart !== undefined || cli.params.close !== undefined){
				let tmux = await connectIdOrAddress(cli.params)
				if(!tmux){
					console.error("> Kmux Server not available.\n> Please run with '-- server background' or '-- server' command line.")
					process.exit(0)
				}

				await tmux.close()
				await async.sleep(500)
			}
			
			if(cli.params.close !== undefined) return process.exit(0)

			if(cli.params.background !== undefined){
				//let args = process.argv.slice(1).filter((a)=> !a.startsWith("--background"))

				let args = new Array<string>()
				args.push(process.argv[1], process.argv[2])
				for(let id in cli.paramsArray){
					if(id == "background") continue

					for(let value of cli.paramsArray[id])
						args.push(`--${id}=${value || ''}`)
				}

				

				let p = ChildProcess.spawn(process.execPath, args, {
					stdio:'ignore',
					detached: true
				})
				p.unref()
				console.info("> Starting server in background")
				process.exit(0)
			}

			let addr = await this.bind(cli.params.id || 'default', cli.params.address || cli.params.bind, cli.params.password)
			console.info("> Server started on:", addr)
			
			return 
		}

		if(cli.params.list !== undefined){


			let keys = ["list", "address", "password", "id"]
			for(let id in cli.params){
				if(keys.indexOf(id) <0)
					return console.error("> Invalid parameter:", id)
			}

			let tmux: TmuxClass
			try{
				tmux = await connectIdOrAddress(cli.params)
			}
			catch(e){

			}
			if(!tmux){
				console.error("> KMUX Server not available.\n> Please run with '-- server background' or '-- server' command line.")
				process.exit(0)
			}
			
			let pros = await tmux.getProcesses()
			process.stdout.write("> Listing processes\n")

			if(!pros.length) console.info("> 0 results")

			for(let i=0;i <pros.length;i++){
				let pro = pros[i]
				let c = '\x1b[32m', d= ' ', rss = ''
				if(pro.status == "offline"){
					c = '\x1b[31m'
					d= ''
				}
				if(pro.memory_usage){
					let mb = 1024*1024
					rss = ` Mem= \x1b[33m${(pro.memory_usage.total / mb).toFixed(2)} MB\x1b[0m`
				}
				console.info(`Status= ${c}${pro.status}\x1b[0m${d} Name= \x1b[33m${pro.id}\x1b[0m  Command= \x1b[34m${pro.cmd}\x1b[0m Arguments= \x1b[34m${pro.args.join(", ")}\x1b[0m${rss}`)
			}


			process.exit(0)
		}

		if(cli.params.kill !== undefined || cli.params.delete !== undefined){
					
			

			let tmux = await connectIdOrAddress(cli.params)
			if(!tmux){
				console.error("> Kmux Server not available.\n> Please run with '-- server background' or '-- server' command line.")
				process.exit(0)
			}

			if(!cli.params.name){
				console.error("> Please specify --name param")
				process.exit(0)
			}

			console.info("> Kill signal sent to process:", cli.params.name)
			if(cli.params.delete !== undefined){
				await tmux.delete(cli.params.name, cli.params.force !== undefined, cli.params.timeout || 10000)
			}
			else{
				await tmux.kill(cli.params.name, {
					signal: cli.params.signal,
					force: cli.params.force !== undefined,
					timeout: cli.params.timeout || 10000
				})
			}
			process.exit(0)
		}


		if(cli.params.restart !== undefined){
			
			

			let tmux = await connectIdOrAddress(cli.params)
			if(!tmux){
				console.error("> Kmux Server not available.\n> Please run with '-- server background' or '-- server' command line.")
				process.exit(0)
			}

			if(!cli.params.name){
				console.error("> Please specify --name param")
				process.exit(0)
			}

			let pro = await tmux.getProcess(cli.params.name)
			if(!pro){
				console.error("> Process not found ")
				process.exit(0)
			}
			let signal = 'SIGTERM'
			if(cli.params.force !== undefined){
				signal = 'SIGKILL'
			}


			let def = new async.Deferred<void>()
			let int = setTimeout(def.reject.bind(def, Exception.create("Timeout waiting response").putCode("TIMEDOUT")), Number(cli.params.timeout || 10000))
			tmux.restart(cli.params.name, signal).then(def.resolve).catch(def.reject)
			await def.promise 
			clearTimeout(int)
			console.info("> Action restart sent to process")
			process.exit(0)
		}

		if(cli.params.start !== undefined){

			

			
			let tmux = await connectIdOrAddress(cli.params)
			if(!tmux){
				console.error("> Kmux Server not available.\n> Please run with '-- server background' or '-- server' command line.")
				process.exit(0)
			}
			
			let name = cli.params.name
			let cmd = cli.params.cmd || cli.params.start
			let args = cli.paramsArray.arg || []
			if(cli.paramsArray.argstr){
				args = cli.params.argstr.split(" ")
			}
			let envarr = cli.paramsArray.env , env = {}
			if(envarr?.length){
				for(let item of envarr){
					let i = item.indexOf("=")
					let name = item.substring(0, i)
					let value = item.substring(i + 1)
					env[name] = value
				}
			}

			if(!cmd){
				console.error("> Please specify --name --cmd params")
				process.exit(0)
			}

			let pro = await tmux.createProcess({
				id: name, 
				cmd, 
				args,
				env,
				autorestart: cli.params["no-autorestart"] === undefined
			})
			await pro.start()
			console.info("> Process", cmd, "started")
			process.exit(0)
		}

		if(cli.params.log !== undefined || cli.params.connect !== undefined){

			
			let tmux = await connectIdOrAddress(cli.params)
			if(!tmux){
				console.error("> Kmux Server not available.\n> Please run with '-- server background' or '-- server' command line.")
				process.exit(0)
			}

			if(!cli.params.name){
				console.error("> Please specify --name param")
				process.exit(0)
			}

			let pro = await tmux.getProcess(cli.params.name)
			if(!pro){
				console.error("> No process available with name: " + String(cli.params.name))
				process.exit(0)
			}
			let info = await pro.info
			if(info.status != "online"){
				console.error("> Process offline")
				process.exit(0)
			}
			console.info("Pid=", await pro.pid, "Name=", (await pro.info).id)
			
			let enumerator = await pro.getAsyncEnumerator(["data","exit"])
			let str = await pro.getLastBuffer()
			if(str){
				process.stdout.write(Buffer.from(str,'base64'))
			}

			let resize = async function(){
				try{
					await pro.resize(process.stdout.columns, process.stdout.rows)
				}catch(e){
					console.error("[ERROR] Failed Resize:", e)
				}
			}

			process.stdout.on("resize", resize)

			process.stdin.setRawMode(true)
			process.stdin.on("data", async (b)=>{
				let text = b.toString('base64')
				let ctrlc = text == "Aw=="
				if(cli.params.name.startsWith("shell")){
					ctrlc = false
				}
				if(text == "BA==" || ctrlc){
					//tmux.destroy()
					console.info("")
					console.info("Disconnected from process console!")
					return process.exit(0)
				}

				try{
					await pro.write(b.toString('base64'))
				}catch(e){
					if(e.code == "RPA_DESTROYED" || e.code == "RPC_DESTROYED"){
						process.exit(2)
					}
				}
			})
			process.stdin.resume()
			
			while(true){
				let item = await enumerator.next()
				if(item.done) break 
				
				if(item.value.type == "data"){
					let bytes = Buffer.from(item.value.data,'base64')
					process.stdout.write(bytes)
				}
				else if(item.value.type == "exit"){
					console.info("> Disconnected, process closed.")
					process.exit(0)
				}
			}

		}



		if(cli.params.save !== undefined){			
			let tmux = await connectIdOrAddress(cli.params)
			if(!tmux){
				console.error("> Kmux Server not available.\n> Please run with '-- server background' or '-- server' command line.")
				process.exit(0)
			}
			await tmux.save()
			console.info("> Process list saved")
			process.exit()
		}

	}



	show(value: any){
		console.dir(value)
	}

	
	static async main(args?: Array<string>){
		try{
			await new Program().main(args)
		}catch(e){
			console.error("Failed execute:", e)
			process.exit(1)
		}
	}


}