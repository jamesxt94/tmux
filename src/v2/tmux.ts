//import uniqid from "gh+/kwruntime/std@1.1.0/util/uniqid.js"

import * as async from "gh+/kwruntime/std@1.1.19/util/async.ts"
import {Exception} from "gh+/kwruntime/std@1.1.19/util/exception.ts"
import fs from "gh+/kwruntime/std@1.1.0/fs/mod.ts"
import {AsyncEventEmitter} from "gh+/kwruntime/std@1.1.19/async/events.ts"

import {child} from './child.ts'

import Os from 'os'
import Path from 'path'
import {Process, ProcessInfo} from './process.ts'
import {ProcessHelper} from './linux/process.ts'

export const shareSymbol = Symbol("share")


let userDataDir = Path.join(Os.homedir(),".kowi")
if(!fs.existsSync(userDataDir)){
	userDataDir = Path.join(Os.homedir(),".kawi", "appData")
	if(!fs.existsSync(userDataDir)){ fs.mkdirSync(userDataDir) }
	fs.symlinkSync(userDataDir, Path.join(Os.homedir(),".kowi"), "junction")
	userDataDir = Path.join(Os.homedir(),".kowi","Tmux")
}
else{
	userDataDir = Path.join(Os.homedir(),".kowi","Tmux")
}

if(!fs.existsSync(userDataDir)){
	fs.mkdirSync(userDataDir)
}
process.setMaxListeners(0)


export interface KillOptions{
	signal?: string 
	force?: boolean
	timeout?: number
}


let spawners = {
	pty: null,
	child,
	childprocess: child
}

export class Tmux extends AsyncEventEmitter{

	id: string 

	#processlist = new Map<string, Process>()
	#started = 0
	
	#pty = null


	constructor(id: string){
		super()
		this.id = id
		this[shareSymbol]  = true

		
		
	}

	async init(){

		
		if(!spawners.pty){
			try{
				spawners.pty = await import('npm://node-pty@1.0.0')
			}catch(e){


				console.info("> Failed install recent 'pty-spawner'. Getting old version", e.message)
				try{
					spawners.pty = await import('npm://node-pty-prebuilt-multiarch@0.10.1-pre.5')
				}catch(e){
					console.error("Failed get pty spawner:", e)
				}
			
			}
		}
		this.#pty = spawners.pty || spawners.child

		if(this.#started) return 

		console.info("> Warning, KMUX will not close on uncaughtExceptions")
		process.on("uncaughtException", function(e){
			console.error("[WARNING] A new exception was not handled")
			console.error(e)
			console.error("-----------------------------------------")
			console.error()
		})

		
		this.#started = Date.now()

		let fname = this.id + "-startup.json"
		let file = Path.join(userDataDir, fname)
		if(fs.existsSync(file)){
			let content = await fs.readFileAsync(file,'utf8')
			let pros = JSON.parse(content)
			pros.sort(function(a, b){
				return a.id > b.id ? 1 : (a.id < b.id  ? -1 : 0)
			})

			for(let pinfo of pros){
				let info = new ProcessInfo()
				Object.assign(info, pinfo)
				info.status = 'offline'
				if(info.autorestart === undefined) info.autorestart = true 

				let pro = this.createProcess(info)
				pro.start()

				// delay 1 second 
				await async.sleep(1000)
			}
		}

		let exited = false
		let gracefullExit = async () => {
			if(exited) return 
			exited = true 
			try{
				await this.close()
			}catch(e){}
			process.exit(1)
		}

		
		process.on("SIGTERM", gracefullExit)
		process.on("SIGINT", gracefullExit)
		process.on("exit", gracefullExit)
		//process.on("SIGCHLD")

		if(Os.platform() == "linux"){
			setTimeout(this.#linuxCheckZombies.bind(this), 5000)
		}
	}

	async delete(id: string, force = false, timeout = 10000){
		// delete process
		let pro = this.#processlist.get(id)
		if(pro){
			await pro.stop(force, timeout)
			this.#processlist.delete(id)
		}		
	}

	async save(){
		// guardar la lista de procesos, para que cuando se inicie automáticamente abra los mismos procesos
		let fname = this.id + "-startup.json"
		let file = Path.join(userDataDir, fname)
		let pros = (await this.getProcesses()) 
		await fs.writeFileAsync(file, JSON.stringify(pros, null, '\t'))
	}


	async close(){
		this.emit("closing")
		let entries = this.#processlist.entries()
		let tasks = new Array<async.DelayedTask<any>>()
		for(let [key, pro] of entries){
			pro.$internal.norestart = true
			tasks.push(new async.DelayedTask<any>(pro.stop(true)))
		}
		while(tasks.length){
			await (tasks.shift().promise)
		}

		setTimeout(function(){
			process.exit(0)
		}, 50)
	}

	async #processExit(pro: Process){
		pro.info.status = 'offline'
		console.info("> Process closed:", pro.info.id, ", Autorestart enabled=", pro.info.autorestart)
		if(pro.info.autorestart && !pro.$internal.norestart){

			this.#processlist.delete(pro.info.id)
			await this.createProcess(pro.info).start()
		}
	}

	createProcess(info: ProcessInfo){
		let pro = this.#processlist.get(info.id)
		if(!pro){

			pro = new Process(this)
			
			info.status = 'offline'
			pro.info = info 
			pro.once("exit", this.#processExit.bind(this, pro))
			this.#processlist.set(info.id,  pro)
		}
		return pro 
	}


	get pty(){
		return this.#pty
	}

	get spawners(){
		return spawners
	}

	selectSpawner(id: string){
		this.#pty = spawners[id]
	}

	async getProcesses(): Promise<ProcessInfo[]>{
		let entries = this.#processlist
		let pros = [] 
		for(let [key, value] of entries){
			pros.push(value.info)
		}
		return pros
	}

	async restart(id: string, signal = 'SIGTERM'){
		let pro = this.#processlist.get(id)
		if(pro){
			pro.$internal.norestart = true 
			await pro.kill(signal)
			this.#processlist.delete(id)
			// recreate 
			pro = this.createProcess(pro.info)
			await pro.start()
			return 
		}
		throw Exception.create(`Process with id: ${id} not found`).putCode("NOT_FOUND")
	}


	async kill(id: string, options: KillOptions | string){
		
		let pro = this.#processlist.get(id)
		if(pro){
			pro.$internal.norestart = true 
			let signal = 'SIGTERM', force = false, timeout = 10000
			if(options){
				if(typeof options == "string"){
					signal = String(options)
				}
				else{
					signal = options.signal 
					force = Boolean(options.force)
					timeout = options.timeout || 10000
				}
			}
			await pro.kill(signal, timeout)
		}
	}

	getMemoryUsage(){
		return process.memoryUsage()
	}

	getProcess(id: string){
		return this.#processlist.get(id)
	}

	getProcessByIndex(i: number){
		return null 
	}

	async destroy(){
		await this.close()
		setImmediate(function(){
			process.exit(50)
		})
	}


	async #linuxCheckZombies(){
		// Sometimes there is zombie processes
		let entries = this.#processlist.entries()
		for(let [key, value] of entries){
			if(value.pid){
				let pid = value.pid
				try{
					let status = await ProcessHelper.getStatus(value.pid)
					let State = (status.State || '').toUpperCase()
					if(State.indexOf("ZOMBIE") >= 0){
						// force emit exit
						value.$markExit()
					}
					else{
						value.info.kernel_status = status
						value.info.memory_usage = await ProcessHelper.getMemoryUsage(pid)
					}
				}catch(e){
					console.error("> Failed get status of PID:", value.pid, e.message)
				}
			}
		}		
		setTimeout(this.#linuxCheckZombies.bind(this), 10000)
	}

}

