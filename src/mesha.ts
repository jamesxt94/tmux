import {Process, Tmux as TmuxClass} from './Tmux.ts'
//import Child from 'child_process'
import * as async from "gh+/kwruntime/std@1.1.4/util/async.ts"
import {Exception} from "gh+/kwruntime/std@1.1.4/util/exception.ts"


import {Server} from 'gitlab://jamesxt94/mesha@b4efe95a/Server.ts'
import {Client} from 'gitlab://jamesxt94/mesha@b4efe95a/Client.ts'
import {ClientSocket} from 'gitlab://jamesxt94/mesha@b4efe95a/ClientSocket.ts'
import {RPC} from 'gitlab://jamesxt94/mesha@b4efe95a/RPC.ts'



import {parse} from 'gitlab://jamesxt94/codes@e72b0bd/cli-params.ts'
import ChildProcess from 'child_process'


export class Program{


	async bind(id: string, address?: string, password?: string){

		let getId = (id: string) => `${process.env.USER}.kmux-${id}`
		let tmux = new TmuxClass(id)
		let shared = function(pass: string){
			if(!password){
				return tmux 
			}else if(password == pass){
				return tmux 
			}
		}

		setTimeout(async function(){
			await tmux.init()
		}, 2000)


		let server = new Server()
		server.on("error", console.error)
		server.on("client", (client:ClientSocket)=>{
			let rpc = new RPC()
			rpc.channel = client
			rpc.init()
			rpc.defaultScope.add("getObject", shared)

			client.on("close", ()=>{
				console.info("Closing client. Current scopes length:", rpc.$scopes.size)
				rpc = null
			})
		})
		
		if(address){
			return await server.bind(address)
		}else{
			return await server.startLocal(getId(id))
		}
	}

	async main(){

		let cli = parse()
		let connect = async (id: string, address?: string, password?: string)=>{
			try{
				let client:Client =null 				
				if(address){
					client = await Client.connect(address)
				}
				else{
					client = await Client.connectLocal(id)
				}
				const rpc = new RPC()
				rpc.channel = client  
				rpc.init()	

				let getObject = await rpc.defaultScope.remote("getObject")
				let tmux: TmuxClass = await getObject.invoke(password)
				return tmux 
			}catch(e){
				if(e.code == "ENOENT" || e.code =="EREFUSED"){
					return null
				}
				throw e 
			}
		}


		let getId = (id: string) => `${process.env.USER}.kmux-${id}`

		if(cli.params.server !== undefined){			

			let keys = ["address","id","bind","password", "restart", "start", "server", "close", "background"]
			for(let id in cli.params){
				if(keys.indexOf(id) <0)
					return console.error("> Invalid parameter:", id)
			}

			if(cli.params.restart !== undefined || cli.params.close !== undefined){
				let tmux = await connect(getId(cli.params.id || "default"), cli.params.address, cli.params.password)
				if(!tmux){
					console.error("> Server not started. Please run with --server --start parameter")
					process.exit(0)
				}

				await tmux.close()
				await async.sleep(500)
			}
			
			if(cli.params.close !== undefined) return process.exit(0)

			if(cli.params.background !== undefined){
				let args = process.argv.slice(1).filter((a)=> !a.startsWith("--background"))
				let p = ChildProcess.spawn(process.execPath, args, {
					stdio:'ignore',
					detached: true
				})
				p.unref()
				console.info("> Starting server in background")
				process.exit(0)
			}

			let addr = await this.bind(cli.params.id || 'default', cli.params.address || cli.params.bind, cli.params.password)
			console.info("> Server started on:", addr)
			return 
		}

		if(cli.params.list !== undefined){

			let keys = ["list", "address", "password", "id"]
			for(let id in cli.params){
				if(keys.indexOf(id) <0)
					return console.error("> Invalid parameter:", id)
			}


			let tmux = await connect(getId(cli.params.id || "default"), cli.params.address, cli.params.password)
			if(!tmux){
				console.error("> Server not started. Please run with --server --start parameter")
				process.exit(0)
			}
			let pros = await tmux.getProcesses()
			process.stdout.write("> Listing processes\n")
			//this.show(pros.map((a)=> [delete a.env, a][1]))
			if(!pros.length) console.info("> 0 results")

			for(let i=0;i <pros.length;i++){
				let pro = pros[i]
				let c = '\x1b[32m', d= ' '
				if(pro.status == "offline"){
					c = '\x1b[31m'
					d= ''
				}
				console.info(`Status= ${c}${pro.status}\x1b[0m${d} Name= \x1b[33m${pro.id}\x1b[0m  Command= \x1b[34m${pro.cmd}\x1b[0m Arguments= \x1b[34m${pro.args.join(", ")}\x1b[0m`)
			}


			process.exit(0)
		}

		if(cli.params.kill !== undefined || cli.params.delete !== undefined){
			
			let tmux = await connect(getId(cli.params.id || "default"), cli.params.address, cli.params.password)
			if(!tmux){
				console.error("> Server not started. Please run with --server --start parameter")
				process.exit(0)
			}

			if(!cli.params.name){
				console.error("> Please specify --name param")
				process.exit(0)
			}

			let times = 0
			let execute = async function(){
				let def = new async.Deferred<void>()
				let int = setTimeout(def.reject.bind(def, Exception.create("Timeout waiting response").putCode("TIMEDOUT")), Number(cli.params.timeout || 10000))
				tmux.kill(cli.params.name, cli.params.signal || "SIGTERM").then(def.resolve).catch(def.reject)
				await def.promise 
				clearTimeout(int)
				console.info("> Signal to close process sent")

				if(cli.params.delete !== undefined){
					
					try{
						def = new async.Deferred<void>()
						int = setTimeout(def.reject.bind(def, Exception.create("Timeout waiting response").putCode("TIMEDOUT")), Number(cli.params.timeout || 10000))
						tmux.delete(cli.params.name).then(def.resolve).catch(def.reject)
						await def.promise 
						clearTimeout(int)
					}catch(e){
						if(e.code == "TIMEDOUT"){
							// force close 
							console.info("> Forzando la salida de proceso")
							times++
							if(times == 4 && (cli.params.force !== undefined)){
								await tmux.forceDelete(cli.params.name)
							}
							else{
								await tmux.kill(cli.params.name, 'SIGKILL')
								await execute()
							}							
						}else{
							throw e
						}
					}
				}
			}

			await execute()
			process.exit(0)
		}

		if(cli.params.restart !== undefined){
			
			let tmux = await connect(getId(cli.params.id || "default"), cli.params.address, cli.params.password)
			if(!tmux){
				console.error("> Server not started. Please run with --server --start parameter")
				process.exit(0)
			}

			if(!cli.params.name){
				console.error("> Please specify --name param")
				process.exit(0)
			}

			let pro = await tmux.getProcess(cli.params.name)
			if(!pro){
				console.error("> Process not found or not started")
				process.exit(0)
			}
			let signal = 'SIGTERM'
			if(cli.params.force !== undefined){
				signal = 'SIGKILL'
			}


			let def = new async.Deferred<void>()
			let int = setTimeout(def.reject.bind(def, Exception.create("Timeout waiting response").putCode("TIMEDOUT")), Number(cli.params.timeout || 10000))
			tmux.restart(cli.params.name, signal).then(def.resolve).catch(def.reject)
			await def.promise 
			clearTimeout(int)
			console.info("> Action restart sent to process")
			process.exit(0)
		}

		if(cli.params.start !== undefined){

			

			let tmux = await connect(getId(cli.params.id || "default"), cli.params.address, cli.params.password)
			if(!tmux){
				console.error("> Server not started. Please run with --server --start parameter")
				process.exit(0)
			}
			
			let name = cli.params.name
			let cmd = cli.params.cmd || cli.params.start
			let args = cli.paramsArray.arg || []
			if(cli.paramsArray.argstr){
				args = cli.params.argstr.split(" ")
			}
			let envarr = cli.paramsArray.env , env = Object.assign({}, process.env)
			if(envarr?.length){
				for(let item of envarr){
					let i = item.indexOf("=")
					let name = item.substring(0, i)
					let value = item.substring(i + 1)
					env[name] = value
				}
			}

			if(!cmd){
				console.error("> Please specify --name --cmd params")
				process.exit(0)
			}

			let pro = await tmux.createProcess(name)
			await pro.start(cmd, args, env)

			console.info("> Process", cmd, "started")
			process.exit(0)
		}

		if(cli.params.log !== undefined || cli.params.connect !== undefined){

			let tmux = await connect(getId(cli.params.id || "default"), cli.params.address, cli.params.password)
			if(!tmux){
				console.error("> Server not started. Please run with --server --start parameter")
				process.exit(0)
			}

			if(!cli.params.name){
				console.error("> Please specify --name param")
				process.exit(0)
			}

			let pro = await tmux.getProcess(cli.params.name)
			if(!pro){
				console.error("> Process not started")
				process.exit(0)
			}
			console.info("Pid=", await pro.getPid(), "Name=", await pro.id)
			
			let enumerator = await pro.getAsyncEnumerator(["data","exit"])
			let str = await pro.getLastBuffer()
			if(str){
				process.stdout.write(Buffer.from(str,'base64'))
			}

			let resize = async function(){
				try{
					await pro.resize(process.stdout.columns, process.stdout.rows)
				}catch(e){
					console.error("[ERROR] Failed Resize:", e)
				}
			}

			process.stdout.on("resize", resize)

			process.stdin.setRawMode(true)
			process.stdin.on("data", async (b)=>{
				let text = b.toString('base64')
				let ctrlc = text == "Aw=="
				if(cli.params.name.startsWith("shell")){
					ctrlc = false
				}
				if(text == "BA==" || ctrlc){
					//tmux.destroy()
					console.info("")
					console.info("Disconnected from process console!")
					return process.exit(0)
				}

				try{
					await pro.write(b.toString('base64'))
				}catch(e){
					if(e.code == "RPA_DESTROYED" || e.code == "RPC_DESTROYED"){
						process.exit(2)
					}
				}
			})
			process.stdin.resume()
			
			while(true){
				let item = await enumerator.next()
				if(item.done) break 
				
				if(item.value.type == "data"){
					let bytes = Buffer.from(item.value.data,'base64')
					process.stdout.write(bytes)
				}
				else if(item.value.type == "exit"){
					console.info("> Disconnected, process closed.")
					process.exit(0)
				}
			}

		}


		


		if(cli.params.save !== undefined){
			
			let tmux = await connect(getId(cli.params.id || "default"), cli.params.address, cli.params.password)
			if(!tmux){
				console.error("> Server not started. Please run with --server --start parameter")
				process.exit(0)
			}
			await tmux.save()
			console.info("> Process list saved")
			process.exit()
		}

	}



	show(value: any){
		console.dir(value)
	}

	
	static async main(){
		try{
			await new Program().main()
		}catch(e){
			console.error("Failed execute:", e)
			process.exit(1)
		}
	}


}