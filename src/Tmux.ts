//import 'gh+/kodhework/kawix@std1.0.1/std/dist/register.js'

import {Channel} from "gh+/kwruntime/std@1.1.6/rpa/channel.ts"
import uniqid from "gh+/kwruntime/std@1.1.0/util/uniqid.js"
import * as async from "gh+/kwruntime/std@1.1.0/util/async.ts"
import fs from "gh+/kwruntime/std@1.1.0/fs/mod.ts"
//import * as pty from 'npm://node-pty-prebuilt-multiarch@0.10.1-pre.4'
import {AsyncEventEmitter} from "gh+/kwruntime/std@1.1.13/async/events.ts"
import {pty, child} from './pty.ts'



import {EventEmitter} from 'events'
import Child from 'child_process'
import Os from 'os'
import Path from 'path'



let userDataDir = Path.join(Os.homedir(),".kowi")
if(!fs.existsSync(userDataDir)){
	userDataDir = Path.join(Os.homedir(),".kawi", "appData")
	if(!fs.existsSync(userDataDir)){ fs.mkdirSync(userDataDir) }
	fs.symlinkSync(userDataDir, Path.join(Os.homedir(),".kowi"), "junction")
	userDataDir = Path.join(Os.homedir(),".kowi","Tmux")
}
else{
	userDataDir = Path.join(Os.homedir(),".kowi","Tmux")
}

if(!fs.existsSync(userDataDir)){
	fs.mkdirSync(userDataDir)
}

const shareSymbol = Symbol("share")


process.setMaxListeners(0)

export class ProcessInfo{
	id: string 
	status: string 
	cmd: string 
	args: string[]
	env: {[key: string]: string}
}

export class Process extends AsyncEventEmitter{
	$p:any
	tmux: Tmux 

	//$func: any
	$cmd = ''
	$args = []
	$env = {}
	$norestart = false
	$started = 0
	$status = new Map<string, string>()

	$b = []
	id: string = uniqid()


	$pty = pty 


	constructor(){
		super()
		this[shareSymbol] = true
	}

	getAsyncEnumerator(event: string | string[]): AsyncGenerator<any, void, unknown> {
		let enum1 = this.getEnumerator(event)
		enum1[shareSymbol] = true 
		return enum1
	}


	$autoRestart(){

		setTimeout(()=>{
			this.emit("restart", {
				cmd: this.$cmd,
				args: this.$args,
				env: this.$env
			})
		}, 4000)

	}

	getStartInfo(){
		return {
			cmd:this.$cmd,
			args: this.$args,
			env: this.$env
		}
	}

	async stop(force = false){
		if(this.$p){
			let p = this.$p, int = undefined, alive = false 
			this.$norestart = true 			
			if(Os.platform() == "win32"){
				alive = this.kill()
			}
			else{
				alive = this.kill('SIGTERM')
				if(force){
					int = setTimeout(function(){
						this.kill('SIGKILL')
					},5000)
				}
			}
			if(alive){
				let def = new async.Deferred<void>()
				this.$p.once("exit", def.resolve)
				await def.promise
			}
			if(int)
				clearTimeout(int)
		}
		else{
			this.emit("exit")
		}
	}

	getStatus(id?:string ){
		if(id){
			return this.$status.get(id)
		}
		else{
			let o:any = {}
			let keys = this.$status.keys()
			for(let key of keys){
				o[key] = this.$status.get(key)
			}
			return o
		}
	}


	selectPTY(type: string){
		if(type == "child" || type == "basic"){
			this.$pty = child
		}
		else{
			this.$pty = pty
		}		
	}


	start(cmd: string, args: string[], env:any = null){

		if(this.$p) return  

		let old_pro = this.tmux.getProcess(this.id)
		if(old_pro && old_pro.$p){
			throw new Error("Failed to start. Process with same name exists")
		}
		
		//console.info("[KMUX] Process starting:", cmd, args.join(", "))
		let event = {
			cmd, 
			args
		}
		this.emit("starting", event)
		this.tmux.emit("process-starting", event)

		this.$cmd = cmd
		this.$args = args
		this.$env = env

		this.$p = this.$pty.spawn(cmd, args, {
			name: 'xterm-color',
			cols: 80,
			rows: 30,
			env: Object.assign({}, process.env, env)
		})


		this.$p.on("error", ()=> {})
		this.$p.on("data", (d)=>{
			let p = Buffer.from(d)
			
			let lines = p.toString().split(/\r?\n/ig)
			for(let line of lines){
				let i = line.indexOf("kmux:status:")
				if(i >= 0){
					let parts = line.substring(i+12).split("$$")[0].split(":")
					this.$status.set(parts[0], parts[1])
					this.emit("status:" + parts[0], parts[1])
					this.tmux.emit("status:"+ parts[0], {
						id: this.id,
						data: parts[1]
					})
				}
			}
			
			this.$b.push(p)
			while(this.$b.length > 200){
				this.$b[0] = null
				this.$b.shift()
			}
			this.emit("data", p.toString('base64'))
		})

		this.$p.on("exit", ()=>{
			delete this.$p
			this.$b = []
			this.emit("exit")

			if(!this.$norestart){
				this.$autoRestart()
			}
		})
	}

	getPid(){
		return this.$p.pid
	}

	kill(signal:string = 'SIGTERM'){
		if(!this.$p){
			this.emit("exit")
			return false
		}

		this.$norestart = true
		try{
			if(Os.platform() == "win32"){
				process.kill(this.$p.pid)
				return true 
			}
			else {
				process.kill(this.$p.pid, signal)
				return true
			}
		}catch(e){

			this.$p .emit("exit")
			return false 
		}
	}


	secureOn(event: string, listener: any){
		this.on(event, listener)
	}


	on(event: string, listener:any){
		let self = this
		if(listener && listener.rpa_preserve){
			let func = null
			func = async function(...args:any[]){

				try{
					return await listener.apply(null, args)
				}catch(e){
					if(e.code == "RPA_DESTROYED"){
						self.removeListener(event, func)
					}
					else{
						//throw e
						console.error("Failed on listener:", e.message)
					}
				}
			}
			super.on(event, func)
		}
		else{
			super.on(event, listener)
		}
		return this
	}

	getLastBuffer(){
		let all = []
		let b = [].concat(this.$b)
		let len = 0, offset = b.length - 1
		while(len < 1000){
			let bu = this.$b[offset]
			offset--
			if(!bu) break
			len += bu.length
			all.push(bu)
		}

		all.reverse()
		let bytes = Buffer.concat(all)
		return bytes.toString('base64')
	}

	write(data: Buffer | string){
		if(!Buffer.isBuffer(data)){
			data = Buffer.from(data,'base64')
		}
		this.$p.write(data)
	}

	resize(cols:number, rows:number){
		this.$p.resize(cols,rows)
	}



}

export class Tmux extends AsyncEventEmitter{
	static version = "1.1.4"
	static originalTmux = null

	

	$serverLocal: Channel = null
	$serverRemote: Channel = null

	$pro = new Map<string,Process>()
	$proStopped = new Map<string,any>()

	id: string
	$started = false 

	constructor(id: string ){

		super()
		this.id = id
		this[shareSymbol]  = true
		/*
		this.init().then(function(){}).catch(function(e){
			console.error(e.message || e)
		})
		*/

	}

	
	getMemoryUsage(){
		return process.memoryUsage()
	}

	async init(){

		if(this.$started) return 
		this.$started = true 

		let fname = this.id + "-startup.json"
		let file = Path.join(userDataDir, fname)
		if(fs.existsSync(file)){
			let content = await fs.readFileAsync(file,'utf8')
			let pros = JSON.parse(content)
			pros.sort(function(a, b){
				return a.id > b.id ? 1 : (a.id < b.id  ? -1 : 0)
			})

			for(let pinfo of pros){
				let pro = this.createProcess(pinfo.id)
				pro.start(pinfo.cmd, pinfo.args, pinfo.env)

				// delay 1 second 
				await async.sleep(1000)
			}
		}

	}

	async delete(id: string){
		let pro = this.$pro.get(id)
		if(pro){
			pro.$norestart = true
			await pro.stop()
		}

		pro = this.$proStopped.get(id)
		if(pro){
			pro.$norestart = true
		}
		this.$proStopped.delete(id)
	}

	async forceDelete(id: string){
		let pro = this.$pro.get(id)
		if(pro){
			pro.$norestart = true
		}
		this.$pro.delete(id)

		pro = this.$proStopped.get(id)
		if(pro){
			pro.$norestart = true
		}
		this.$proStopped.delete(id)
	}


	async save(){
		// guardar la lista de procesos, para que cuando se inicie automáticamente abra los mismos procesos
		let fname = this.id + "-startup.json"
		let file = Path.join(userDataDir, fname)
		let pros = (await this.getProcesses()) //.filter((a)=> a.status == 'online')
		await fs.writeFileAsync(file, JSON.stringify(pros, null, '\t'))
	}

	async close(){
		let keys = this.$pro.keys()
		let tasks = []
		for(let key of keys){
			let pro = this.$pro.get(key)
			tasks.push(new async.DelayedTask<any>(pro.stop()))
		}
		while(tasks.length){
			await (tasks.shift().promise)
		}
		setTimeout(function(){
			process.exit(0)
		}, 50)

	}


	getVersion(){
		return Tmux.version
	}

	getSourceFile(){
		return __filename
	}



	async hotUpdate(url: string){

		let mod = await KModule.import(url || __filename, {
			force: true
		})
		if(mod.Tmux){
			
			//let items = ["hotUpdate","createProcess","getProcesses","kill","getProcess", "getProcessByIndex",
			//	"localServer","remoteServer", "checkLocalServer", "detachedLocalServer", "hasLocalServer", "hasRemoteServer", "destroy"]
			let items = Object.getOwnPropertyNames(mod.Tmux.prototype)
			let cTmux = Tmux.originalTmux || Tmux
			mod.Tmux.originalTmux = cTmux
			for(let item of items){
				cTmux.prototype[item] = mod.Tmux.prototype[item]
			}
		}

		if(mod.Process){
			//let items = ["start","getPid","kill","secureOn","on","getLastBuffer","write","resize"]

			let items = Object.getOwnPropertyNames(mod.Process.prototype)
			for(let item of items){
				Process.prototype[item] = mod.Process.prototype[item]
			}
		}

	}

	createProcess(id: string = ''){
		if(id){
			let px = this.$pro.get(id)
			if(px) return px
		}
		let p = new Process()
		if(id) p.id = id
		p.tmux = this 

		this.$pro.set(p.id, p)
		this.$proStopped.delete(p.id)

		p.on("exit", ()=>{
			this.$pro.delete(p.id)
			this.$proStopped.set(p.id, p)
		})
		p.once("restart", (info)=>{
			if(p.$norestart) return

			let pro = this.createProcess(id)
			pro.start(info.cmd, info.args, info.env)
		})
		return p
	}

	async getProcesses(): Promise<ProcessInfo[]>{
		let processes = []
		let keys = this.$pro.keys()
		for(let key of keys){
			let pro = this.$pro.get(key)
			processes.push({
				id: pro.id,
				status: 'online',
				cmd: pro.$cmd,
				args: pro.$args,
				env: pro.$env
			})
		}

		keys = this.$proStopped.keys()
		for(let key of keys){
			let pro = this.$proStopped.get(key)
			processes.push({
				id: key,
				status: 'offline',
				cmd: pro.$cmd,
				args: pro.$args,
				env: pro.$env
			})
		}
		return processes //Channel.plain(processes)
	}






	async restart(id: string, signal:string = 'SIGTERM'){
		let pro = this.$pro.get(id)
		if(pro){
			await pro.stop()
		}

		let info = pro.getStartInfo()
		let newPro  = this.createProcess(id)
		newPro.start(info.cmd, info.args, info.env)
	}


	kill(id: string, signal:string = 'SIGTERM'){
		let p = this.$pro.get(id)
		if(p){
			if(Os.platform() == "win32"){
				return p.kill()
			}
			else{
				return p.kill(signal)
			}
		}
	}

	getProcess(id: string){
		return this.$pro.get(id)
	}

	getProcessByIndex(i: number){
		let keys = [...this.$pro.keys()]
		return this.$pro.get(keys[i])
	}


	async localServer(){
		let user= process.env.KMUX_USER || process.env.USER || process.env.USERPROFILE
		this.$serverLocal = await Channel.registerLocal("tmux-" + user + "-" + this.id + "-server", this)
		/*
		setInterval(()=>{
			console.info("VARS:",this.$serverLocal.$vars)
		}, 4000)
		*/
	}

	async remoteServer(password: string, hostname: string, port: number){
		let self = this
		// protect by password
		let cid = "tmux-server"
		this.$serverRemote = await  Channel.registerRemote(cid, {
			get(pass: string){
				if(pass == password){
					return self
				}
				else{
					throw new Error("Access denied. You cannot connect to service.")
				}
			}
		}, port, hostname)
	}


	async checkLocalServer(){
		// detectar si ya hay un localServer creado
		if(this.hasLocalServer()) return this
		try{
			return await Tmux.connect(this.id)
		}
		catch(e){
			console.log("> Local server lookup:", e.message)
			if(e.code == "RPA_UNAVAILABLE")
				return null
		}
		return null
	}



	async detachedLocalServer(){
		//if( await this.checkLocalServer()) return
		let p = Child.spawn(process.argv[0], [process.argv[1], __filename], {
			detached: true,
			stdio:'inherit',
			env: Object.assign({},process.env,{
				DETACHED_LOCAL_SERVER: this.id
			})
		})
		p.unref()
	}

	get RPALocalServer(){
		return this.$serverLocal
	}

	get RPARemoteServer(){
		return this.$serverRemote
	}

	hasLocalServer(){
		return Boolean(this.$serverLocal)
	}

	hasRemoteServer(){
		return Boolean(this.$serverRemote)
	}


	async destroy(){
		let keys = this.$pro.keys()
		for(let key of keys){
			this.$pro.get(key).kill()
		}
		setImmediate(function(){
			process.exit(0)
		})

	}

	static server(id:string){
		let tmux = new Tmux(id)
		return tmux.localServer()
	}



	static async connect(id:string){
		let user= process.env.KMUX_USER || process.env.USER || process.env.USERPROFILE
		let channel = await Channel.connectLocal("tmux-"+ user + "-" + id + "-server")
		return channel
	}

	static async connectRemote(port:number, host: string, password: string){
		let user= process.env.KMUX_USER || process.env.USER || process.env.USERPROFILE
		let channel = await Channel.connectRemote("tmux-server", port, host)
		channel.client =  await channel.client.get(password)
		return channel
	}
}

export var kawixDynamic = {
	time: 15000
}


if(process.env.DETACHED_LOCAL_SERVER){
	let tmux = new Tmux(process.env.DETACHED_LOCAL_SERVER)
	delete  process.env.DETACHED_LOCAL_SERVER
	tmux.localServer()
}
