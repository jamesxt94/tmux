import {Tmux} from '../src/Tmux.ts'

// TEST SERVICE
main()
async function main(){

	let tmux: Tmux
	let _tmux = new Tmux("default")
	let channel = await _tmux.checkLocalServer()
	if(!channel){
		await _tmux.detachedLocalServer()
		let time = Date.now()
		while(Date.now() - time < 10000){
			try{
				channel = await Tmux.connect("default")
				break
			}catch(e){}
		}
		if(!channel) console.error("Failed waiting tmux server")
	}
	tmux = channel.client

	let pro = await tmux.getProcess("command0")
	if(!pro){
		// create process?
		pro = await tmux.createProcess("command0")
		await pro.start("fish", [])

	}
	console.info("PID connected:", await pro.getPid())

	let data = Buffer.from(await pro.getLastBuffer(),'base64')
	process.stdout.write(data)

	pro.on("data", function(base64){
		let bytes = Buffer.from(base64,'base64')
		process.stdout.write(bytes)
	})

	pro.on("exit", function(){
		console.info("Process killed")
		process.exit(1)
	})

	await pro.resize(process.stdout.columns, process.stdout.rows)
	process.stdout.on("resize", function(){
		console.info(arguments)
	})
	process.stdin.setRawMode(true)
	process.stdin.on("data", async (b)=>{
		let text = b.toString('base64')
		if(text == "BA=="){
			//tmux.destroy()
			console.info("Disconnected!")
			process.exit(0)
			return
		}

		pro.write(b.toString('base64'))
	})
	process.stdin.resume()

}
