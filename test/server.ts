import {Tmux} from '../Tmux'

main()
async function main(){
	let tmux = new Tmux("default")
	if(await tmux.checkLocalServer()){
		console.info("Local server is ok!")
		process.exit()
	}
	tmux.detachedLocalServer()
	//let pro = await tmux.createProcess()
	//console.info(tmux.getProcessByIndex(0))
	//pro.start("bash",[])
}
